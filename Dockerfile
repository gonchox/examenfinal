FROM openjdk:17

WORKDIR /app

COPY ./target/final-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "final-0.0.1-SNAPSHOT.jar"]