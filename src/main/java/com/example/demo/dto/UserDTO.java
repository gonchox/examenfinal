package com.example.demo.dto;

import com.example.demo.model.Transaction;
import lombok.Data;

import java.util.List;

@Data
public class UserDTO {
    private String name;
    private String dni;
    private String phone;
    private List<Transaction> transactions;
}
