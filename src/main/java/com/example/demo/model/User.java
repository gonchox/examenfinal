package com.example.demo.model;

import com.example.demo.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    private String id;
    private String name;
    private String dni;
    private String phone;
    private List<Transaction> transactions;

    public User(UserDTO userDTO){
        this.name = userDTO.getName();
        this.dni= userDTO.getDni();
        this.phone = userDTO.getPhone();
        this.transactions = userDTO.getTransactions();
    }
}
