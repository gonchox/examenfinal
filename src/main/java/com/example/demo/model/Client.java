package com.example.demo.model;

import lombok.Data;

@Data
public class Client {
    private String dni;
    private String accNumber;
    private String accType;
}
