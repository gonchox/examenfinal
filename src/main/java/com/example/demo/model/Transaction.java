package com.example.demo.model;

import lombok.Data;

@Data
public class Transaction {
    private String type;
    private double quantity;
    private Client client;
}
