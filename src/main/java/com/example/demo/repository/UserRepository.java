package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.List;

public interface UserRepository {
    User saveUser(User user);
    List<User> getUsers();
    User getUserById(String id);
}
