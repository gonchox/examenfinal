package com.example.demo.repository.Impl;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public User saveUser(User user) {
        mongoTemplate.insert(user, "User");
        return user;
    }

    @Override
    public List<User> getUsers() {
        return mongoTemplate.findAll(User.class, "User");
    }

    @Override
    public User getUserById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        return  mongoTemplate.findOne(query,User.class,"User");
    }
}
