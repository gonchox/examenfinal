package com.example.demo.controller;

import com.example.demo.dto.UserDTO;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable String id){
        try{
            User user = userService.getUserById(id);
            return ResponseEntity.status(HttpStatus.OK).body(user);

        }catch (Exception ex){
            throw ex;
        }
    }

    @GetMapping("")
    public ResponseEntity<List<User>> getUsers(){
        try{
            List<User> users = userService.getAllUsers();
            return ResponseEntity.status(HttpStatus.OK).body(users);

        }catch (Exception ex){
            throw ex;
        }
    }

    @PostMapping("")
    public ResponseEntity<User> createUser(@RequestBody UserDTO userDTO){
        try{
            User user = userService.createUser(userDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(user);

        }catch (Exception ex){
            throw ex;
        }
    }
}
