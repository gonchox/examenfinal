package com.example.demo.service.Impl;

import com.example.demo.dto.UserDTO;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {

        List<User> users = userRepository.getUsers();
        return users;
    }

    @Override
    public User createUser(UserDTO userDTO) {
        User user = new User(userDTO);
        userRepository.saveUser(user);
        return user;
    }

    @Override
    public User getUserById(String id) {
        return userRepository.getUserById(id);
    }
}
